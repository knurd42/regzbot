# Regression tracking bot designed for the Linux kernel

Regzbot is a bot tailored for low-overhead regression tracking in the email
driven Linux kernel development process. It's actually used in the field, but
still in a alpha stage: more adjustments are needed to make it better suite the
its intended purpose.

To get an impression how regression tracking with regzbot is performed in
practice, see the
[about page for the Linux kernel regression tracking efforts](https://linux-regtracking.leemhuis.info/about/)
and
[the list of regressions regzbot currently tracks](https://linux-regtracking.leemhuis.info/regzbot/mainline/).
If you want to interact with regzbot, check out
[getting started with regzbot](docs/getting_started.md) or the bots
[reference documentation](docs/reference.md).

To install or develop for regzbot, see the [installation documentation](docs/installation.md).

## Licensing

Rezbot is available under the APGL 3.0; see the file COPYING for details. If
you think a more liberal license should be used, let regzbot's author know what
you'd prefer, as for now it's still quite easy to change the license.

Regzbot was started by Thorsten Leemhuis as part of a project that has received
funding from the European Union’s Horizon 2020 research and innovation
programme under grant agreement No 871528.

Since May 2022 regzbot development and the Linux kernel regression tracking
efforts performed by Thorsten are supported with funds from Meta.
